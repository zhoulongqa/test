# -*- coding: utf-8 -*-
from base.BaseTest import BaseTest
from pages.toutiao.MainPage import MainPage
from pages.toutiao.MinePage import MinePage
from pages.toutiao.MyCategoryPage import MyCategoryPage, add_category_header
from utils import Logger
from utils import ReadConf
from utils.UIHelper import TTBy


def is_main_page(instance):
    """判断是否为首页"""
    Logger.INFO("check if it is main_page")
    return instance.uihelper.exist(MainPage['Feed流列表'], timeout=12)


def wait_main(instance, times=3):
    """
    等待主界面加载
    :param instance: case instance 
    :param times: relaunch times
    :return: 
    """
    Logger.INFO("check if it is main_page with relaunching several times")
    while times>0:
        if is_main_page(instance):
            return True
        else:
            instance.uihelper.restart_app()
            times-=1
    return False



def login(instance):
    """登录流程"""
    Logger.INFO("start login method")
    filename = "AppiumConfig.json"
    config_dict = ReadConf.ReadConf(filename)
    phone_number = config_dict["account"]
    password = config_dict["password"]
    account_name = config_dict["account_name"]

    instance.uihelper.click_element(MainPage["未登录"])
    if instance.uihelper.exist((account_name, TTBy.TEXT)):
        Logger.INFO("already logged in")
    else:
        #登录了其他账号的情况先不考虑
        Logger.INFO("start log in")
        instance.uihelper.click_element(MinePage["电话登录"])
        instance.uihelper.click_element(MinePage["账号密码登录"])
        instance.uihelper.enter(MinePage["手机号输入框"], phone_number)
        instance.uihelper.enter(MinePage["密码输入框"], password)
        instance.uihelper.click_element(MinePage["进入头条"])
        # verify
    return instance.uihelper.exist((account_name, TTBy.TEXT))



def change_category_in_my_category_page(instance, channel_name):
    """
    进入我的频道页面切换频道到 channel_name
    :param instance: case instance
    :return: True/False
    """
    instance.uihelper.click_element(MainPage["更多频道"])
    instance.uihelper.exist(MyCategoryPage["GridView"])
    if instance.uihelper.exist((channel_name, TTBy.TEXT)):
        # 我的频道 中有 该频道
        return instance.uihelper.click_element((channel_name, TTBy.TEXT))
    if instance.uihelper.exist((add_category_header + channel_name, TTBy.TEXT)):
        # 频道推荐 中有 该频道
        instance.uihelper.click_element((add_category_header + channel_name, TTBy.TEXT))
        return instance.uihelper.click_element((channel_name, TTBy.TEXT))
    # 下滑直到找到该频道
    element_exist = False
    times = 5
    while not element_exist and times > 0 :
        instance.uihelper.hardware_helper.scroll_up_one_page()
        times-=1
        element_exist = instance.uihelper.exist((add_category_header + channel_name, TTBy.TEXT))
    if element_exist:
        instance.uihelper.click_element((add_category_header + channel_name, TTBy.TEXT))
        instance.uihelper.hardware_helper.scroll_down_one_page()
        instance.uihelper.click_element((channel_name, TTBy.TEXT))
        return True
    else:
        return False













