# -*- coding: utf-8 -*-
from base.BaseTest import BaseTest
import unittest
from utils.eventutils import EventCheck
from pages.toutiao.MinePage import MinePage
from pages.toutiao.wenda.WendaDetailPage import WendaDetailPage
from pages.toutiao.wenda.WendaListPage import WendaListPage
from pages.toutiao.wenda.WendaChannel import WendaChannel
from testcases.toutiao.CommonProcess import PublicProcess
from testcases.toutiao.CoreEvents.wenda import *


class event_wenda_enter_list(BaseTest):
    def setUp(self):
        """
        test
        :return:
        """
        PublicProcess.wait_main(self)
        PublicProcess.change_category_in_my_category_page(self, wenda)



    def tearDown(self):
        """
        test2
        :return:
        """

    @EventCheck.check_step(event='event_wenda_enter_list', wait_time=5)
    def test_case(self):
        self.uihelper.click_element(WendaChannel["普通问答"])




if __name__ == "__main__":
    unittest.main(verbosity=2)