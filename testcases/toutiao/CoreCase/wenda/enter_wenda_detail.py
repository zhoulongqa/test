# -*- coding: utf-8 -*-
import unittest
from base.BaseTest import BaseTest
from testcases.toutiao.CommonProcess import PublicProcess
from testcases.toutiao.CommonProcess.UGCProcess import UGCProcess
from pages.toutiao.wenda.WendaChannel import WendaChannel
from pages.toutiao.wenda.WendaListPage import WendaListPage
from pages.toutiao.wenda.WendaDetailPage import WendaDetailPage
from testcases.toutiao.CoreCase.wenda import wenda


class enter_wenda_detail(BaseTest):
    def setUp(self):
        """
        test
        :return:
        """


    def tearDown(self):
        """
        test2
        :return:
        """

    def test_enter_wenda_detail(self):
        PublicProcess.change_category_in_my_category_page(self, wenda)
        self.uihelper.click_element(WendaChannel["普通问答"])
        self.uihelper.click_element(WendaListPage["回答列表"])
        result = self.uihelper.exist(WendaDetailPage["悟空问答"])
        self.assertTrue(result, "did not enter wenda detail page")





if __name__ == "__main__":
    unittest.main(verbosity=2)