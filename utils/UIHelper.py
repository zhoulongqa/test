# -*- coding: UTF-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
# __author__ = 'guohao'
import json
import re
import subprocess
import shlex
from threading import Timer
from difflib import SequenceMatcher
import selenium.common.exceptions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions
from appium.webdriver.common.mobileby import MobileBy
from appium.webdriver.common.touch_action import TouchAction
import subprocess
import unittest
import os
from utils import Logger
import time
from os import path
basedir = path.realpath(path.join(path.dirname(__file__), '..'))
from utils.HardwareHelper import HardwareHelper

class TTBy(MobileBy):
    """
    add by text
    as for content-desc, use accesibility_id
    """
    TEXT = 'TEXT'



class UIHelper(object):
    """
    Usage:UI
        help user to find element friendly,
        just forgot appium find element method, such as:
        find_element_by_name, find_element_by_xpath,
        find_element_by_accessibility_id
        whatever. we provide one method "find_element" to replace
        appium several find element functions.

        it makes find element more easily
    """

    def __init__(self, appium_driver, url_service, exception_check_json=""):
        """
        :param appium_driver: appium driver
        :param url_service: URL Scheme service
        :param popup_elements: popup elements
        """

        self.log = Logger
        self.driver = appium_driver
        self.hardware_helper = HardwareHelper(self.driver)
        self.exception_check_json = exception_check_json

        self.url_service = url_service

    @staticmethod
    def abs_page(source):
        x = re.findall('>\n(\s+<\w+)', source)
        v = [val.strip().replace("<", str(val.count(" "))) for idx, val in enumerate(x)]
        z = [value for idx, value in enumerate(v[:-1]) if v[idx] != v[idx + 1]]
        return "".join(z)

    @staticmethod
    def diff_page(page1, page2):
        return SequenceMatcher(None, UIHelper.abs_page(page1), UIHelper.abs_page(page2)).real_quick_ratio()

    def find_element(self, text ,time=5, parent_index=0):
        """
        查找祝控件
        :param text: 控件定位方式(value, by)
        :param time: timeout
        :param index1 父控件定位取的index
        :return: elements or None
        """
        self.log.INFO("search_element: {}".format(text))
        if isinstance(text, tuple) and len(text) == 4:
            # 父控件定位子控件
            father_text = (text[0], text[1])
            child_text = (text[2], text[3])
            father_ele = self.find_element_once(father_text, time=time, parent_ele=None)
            # 父控件为空，则返回None
            if not father_ele:
                Logger.INFO("father element does not exist")
                return None
            child_ele = self.find_element_once(child_text,time=time, parent_ele=father_ele[parent_index])
            return child_ele
        else:
            # driver定位自控件
            return self.find_element_once(text, time=time)


    def find_element_once(self, text, time=5, parent_ele=None):
        """支持直接查找 和 父控件查找子控件"""
        if parent_ele:
            total_driver = parent_ele
        else:
            total_driver = self.driver

        self.log.INFO("search_element: {}".format(text))
        # get value an by
        if isinstance(text, str):
            value = text
            if text.startswith("/"):
                by = TTBy.XPATH
            else:
                by = TTBy.ID
        elif isinstance(text, tuple) and len(text) == 2:
            by_value = text[0]
            by = text[1]
            if by == TTBy.TEXT:
                value = 'new UiSelector().text("' + by_value + '")'
                by = TTBy.ANDROID_UIAUTOMATOR
            else:
                value = by_value
        else:
            raise BaseException("incorrect syntax in pages module")
        # find target_element based on by and value
        try:
            target_element = WebDriverWait(total_driver, time).until(
                expected_conditions.presence_of_all_elements_located((by, value)))
            return target_element
        except BaseException as e:
            Logger.INFO("find element fail for : {}".format(e.message))
            return None


    def find_element_with_popup(self, text, time=5):
        el = self.find_element(text, time)
        if not el:
            while self.popup_window_detect_and_click():
                el = self.find_element(text, time)
                if el:
                    return el
        return el


    def popup_window_detect_and_click(self, text=""):
        """
        :param text: 
        :return: True/False
        """
        self.log.INFO("start handle alert")
        source = self.driver.page_source
        exception_info = self.load_exception_elements()
        for key in exception_info:
            self.log.INFO("checking if popup_element: {} exist".format(key))
            if key in source:
                self.log.INFO("popup_element {} is found".format(key))
                text = (exception_info[key], TTBy.NATIVE_TEXT)
                exception_btn = self.find_element_with_popup(text, time=5)

                if exception_btn:
                    exception_btn[0].click()
                    time.sleep(2)
                    self.log.INFO(exception_info[key] + ' -- >弹窗处理完成')
                    return True
                else:
                    self.log.INFO('没有找到这个弹框控件： ' + exception_info[key])
                    return False


    def load_exception_elements(self):
        print(self.exception_check_json)
        return json.load(open(basedir + '/configs/' + self.exception_check_json))['popupElements']


    def exist(self, text, timeout = 5):
        """
        :param text: 
        :return: True/False
        """
        if self.find_element_with_popup(text, timeout):
            self.log.INFO(str(text) + " exist")
            return True
        else:
            self.log.INFO(str(text) + " doesn't exist")
            return False

    def click_element(self, text, time=5, index=0):
        """
        :rtype: object
        """
        elements = self.find_element_with_popup(text, time)
        if elements:
            elements[index].click()
        else:
            self.log.ERROR("控件：" + str(text) + ",获取不到")
            self.get_screenshot()
            raise BaseException("click element {} fail".format(text))

    def click_element_coordinate(self, text, time=5, index=0, x_per=0.5, y_per=0.5):
        """
        click element by its coordinate loaction
        :param text: the target element text
        :rtype: object
        """
        elements = self.find_element(text, time=time)
        if elements:
            rect = dict(elements[index].size.items() + elements[index].location.items())
            x = rect['x'] + rect['width'] * x_per
            y = rect['y'] + rect['height'] * y_per
            return self.driver.tap([(x, y)])
        else:
            self.log.ERROR("控件：" + text + ",获取不到")
            self.get_screenshot()
            raise unittest.TestCase.failureException


    # 获取控件的text
    def get_element_text(self, id, index=0):
        elements = self.find_element(id)
        if elements:
            text1 = elements[index].get_attribute('name').strip()
            return text1
        else:
            self.log.ERROR("控件：" + id + ",获取不到")
            self.get_screenshot()
            raise unittest.TestCase.failureException

    def get_element_value(self, text, id, index=0):
        elements = self.find_element(text, id)
        if elements:
            text1 = elements[index].get_attribute('value').strip()
            return text1
        else:
            self.log.ERROR("控件：" + id + ",获取不到")
            self.get_screenshot()
            raise unittest.TestCase.failureException

    # 截屏
    def get_screenshot(self):
        # 判断文件是否存在，若不存在，创建
        datefile = basedir + '/testcases/screenshot'
        if not os.path.exists(datefile):
            os.mkdir(datefile)
        now = time.strftime("%Y-%m-%d-%H-%M-%S", time.localtime(time.time()))
        filename = datefile + '/' + now + ".png"
        time.sleep(1)
        self.driver.get_screenshot_as_file(filename)
        print('screenshot:' + now + '.png')


    # 重启App
    def restart_app(self):
        self.driver.close_app()
        self.driver.launch_app()

    # 判断结果是否符合预期
    def assert_true(self, expr, msg=None):
        if not expr:
            self.get_screenshot()
            raise unittest.TestCase.failureException(msg)
        else:
            return True

    # 相等
    def assert_equal(self, first, second, msg=None):
        if first != second:
            self.get_screenshot()
            mes = str(first) + '的值不等于' + str(second) + '的值'
            raise unittest.TestCase.failureException(msg)

    # 两个数值大于/等于
    def assert_greaterequal(self, first, second, msg=None):
        if not first >= second:
            self.get_screenshot()
            mes = str(first) + '的值不等于或大于' + str(second) + '的值'
            raise unittest.TestCase.failureException(msg)

    # 在输入框中输入内容
    def enter(self, id, text, index=0):
        elements = self.find_element(id)
        if elements:
            elements[index].clear()
            elements[index].set_value(text)
        else:
            self.log.ERROR("控件：" + id + ",获取不到")
            self.get_screenshot()
            raise unittest.TestCase.failureException

    def send_key_event(self, text):
        time.sleep(1)
        self.driver.press_keycode(text)

    @staticmethod
    def show_object(obj):
        try:
            el_info = " ".join(['text:{text},'.format(text=obj.text),
                                'tag_name: {tag_name},'.format(tag_name=obj.tag_name),
                                'enabled: {enabled},'.format(enabled=obj.is_enabled()),
                                'displayed:{displayed}'.format(displayed=obj.is_displayed())])

        except Exception as e:
            print("获取元素信息失败")

    @staticmethod
    def element_to_string(ios_element, idx):

        if ios_element.name:
            return ios_element.name

        return ios_element.cls_name + " index: " + str(idx)

    def get_all_elements_info(self):
        all_elements_info = []
        el_text = self.driver.page_source

        for text in el_text:
            name = ""
            el_type = re.findall(u'type="(\w+)"\s', text)[0]
            name_info = re.findall(u'name="(.+?)"\s', text)

            # 控件的文字描述
            if name_info:
                name = name_info[0]

            # 特殊的组件，比如iOS状态栏，等，不需要遍历
            if 'XCUIElementTypeOther' in el_type:
                continue

            el_x = int(re.findall(u'x="([\d|-]+)"', text)[0])
            el_y = int(re.findall(u'y="([\d|-]+)"', text)[0])
            el_width = int(re.findall(u'width="([\d|-]+)"', text)[0])
            el_height = int(re.findall(u'height="([\d|-]+)"', text)[0])

            all_elements_info.append(IOSElementInfo(el_type, el_x, el_y, el_height, el_width, name))

        return all_elements_info

    def is_valid_element(self, el, el_info):

        # iOS 屏幕尺寸
        if el_info.y > 623:
            self.log.INFO("超出当前页面的元素")
            return False

        if 'back' in el_info.name:
            self.log.INFO("可能是返回按钮")
            return False

        # if not el.is_enabled():
        #     self.log.INFO("控件不展示")
        #     return False

        return True

    def draw_element(self, el):
        pass

    def go_page(self, url):
        time.sleep(5)
        self.log.INFO('----- go page ------')
        before_jump = self.driver.page_source
        self.url_service.jump_to_page(url)
        time.sleep(1)
        for i in range(0, 3):
            after_jump = self.driver.page_source

            if after_jump == before_jump:
                time.sleep(5)
            else:
                break

    @staticmethod
    def get_local_device():
        text = subprocess.Popen('ios-deploy -l',
                                shell=True, stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE).stdout.read().strip()
        device_id = re.findall(r'(\w{40})', str(text))
        return device_id[0]

    def get_element_values(self, id, index=0):
        elements = self.find_element(id)
        if elements:
            text1 = elements[index].get_attribute('value')
            return text1
        else:
            self.log.ERROR("控件：" + id + ",获取不到")
            self.get_screenshot()
            raise unittest.TestCase.failureException


class IOSElementInfo:
    def __init__(self, cls_name, x, y, height, width, name=""):
        self.cls_name = cls_name
        self.name = name
        self.x = x
        self.y = y
        self.height = height
        self.width = width
