# -*- coding: UTF-8 -*-
import json
import time
import os
import sys
from appium import webdriver
from os import path
from utils import Logger
from utils.ReadConf import ReadConf
basedir = path.realpath(path.join(path.dirname(__file__), '..'))


class AppiumHelper(object):
    def __init__(self):
        self.appium_driver = None
        self.Logger = Logger

    def get_appium_config(self, platform="Android", product="Toutiao"):
        """从json配置文件中读取desired capabilities配置"""
        obj = ReadConf("AppiumConfig.json")
        if product is not None:
            obj = obj[product]
        if platform is not None:
            obj = obj[platform]
        return obj


    def get_new_webdriver(self, platform="Android", product="Toutiao", udid=None, addr='127.0.0.1:4723'):
        self.Logger.INFO("Connecting to Appium Server")
        cap = self.get_appium_config(platform=platform, product=product)
        if cap is None:
            raise BaseException('get_new_webdriver:cap is None')
        if udid is not None:
            cap['udid'] = udid
        addr = 'http://{0}/wd/hub'.format(addr)
        self.appium_driver = webdriver.Remote(addr, cap)
        self.Logger.INFO("Connected to Appium Server Success")
        return self.appium_driver


    def close_driver(self):
        if self.appium_driver is not None:
            self.appium_driver.quit()


    def start_appium_server(self, appium_port=4723, device="", bp=10000):
        Logger.INFO('-----------------start_appium_server--------------------')
        os.system(
            "appium --log-timestamp -a 127.0.0.1 -p %s -U %s -bp %s > %s_log.txt" % (appium_port, device, bp, device))


if __name__ == '__main__':
    from utils import CommFun
    helper = AppiumHelper()
    import multiprocessing

    for a in range(0, 20):
        Logger.INFO("---------------------------------->%s" % (10000 + a))
        appium_process = multiprocessing.Process(target=helper.start_appium_server, args=(10000 + a, 10000 + a))
        appium_process.start()
        CommFun.sleep(5)

    CommFun.sleep(18)
    sys.exit(1)