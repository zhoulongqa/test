# -*- coding: UTF-8 -*-
import json
from os import path
basedir = path.realpath(path.join(path.dirname(__file__), '..'))

def ReadConf(filename):
    fh = open(basedir + "/configs/" + filename)
    config_dict = json.load(fh)
    return config_dict


def ReadConfWithKey(filename, key):
    fh = open(basedir + "/configs/" + filename)
    config_dict = json.load(fh)
    if key not in config_dict.keys():
        raise BaseException("ReadConfWithKey Error, key not exist")
    else:
        value = config_dict[key]
    return value


if __name__ == '__main__':
    result = ReadConfWithKey("AppiumConfig.json", "account")
    print type(result)
    print result
