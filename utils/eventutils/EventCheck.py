# -*- coding: UTF-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import os
from os import path
basedir = path.realpath(path.join(path.dirname(__file__), '../..'))
import utils.eventutils.EventHttpHandler as event_handler
from utils.eventutils.EventHttpHandler import *
from utils.eventutils.Verifier import Verifier
from utils import CommFun
import json
import unittest

if 'testcases' in path.abspath('.'):
    filepath = path.abspath('../../../..')
else:
    filepath = path.abspath('.')


def check_step(*dargs, **dkargs):
    """
    :param dargs:
    :param dkargs: event wait_time
    :return: decoration
    """
    def wrapper(func):
        def _wrapper(*args, **kwargs):
            Logger.INFO("<-----Start Executing Event Case---->")
            sever_flag = os.getenv('EVENTSERVER')
            if not sever_flag:
                EventHttpServer.PORT = 10303
                local_ip = CommFun.get_local_ip()
                EventHttpServer.start_server(local_ip)
            else:
                print(u'已有服务启动')
            start_observer()
            Logger.INFO("<-----Run Action---->")
            func(*args, **kwargs)
            wait_for_event(dkargs['wait_time'])
            stop_observer()
            schema_json = get_expect_event(dkargs['event'])
            expect_schema = schema_json['expect_event']
            case = schema_json['case']
            result = verify_event_multi_without_count(expect_schema)
            result['case'] = case
            if not result['success']:
                Logger.ERROR(str(result))
                raise unittest.TestCase.failureException
            else:
                Logger.INFO(str(result))
        return _wrapper
    return wrapper


def start_server():
    print '----------------start_server----------------------'
    server = EventHttpServer(('0.0.0.0', EventHttpServer.PORT), EventHttpHandler)
    ip, port = server.server_address
    server_thread = threading.Thread(target=server.serve_forever)
    server_thread.setDaemon(True)
    server_thread.start()


def start_observer():
    print '---------------','start_observer'
    EventHttpHandler.start_observer()


def stop_observer():
    print '---------------', 'stop_observer'
    EventHttpHandler.stop_observer()


def wait_for_event(wait_time):
    CommFun.sleep(wait_time)


def get_received_events():
    return EventHttpHandler.received_events


def _clear_received_event():
    EventHttpHandler.clear_events()


def get_expect_event(events):
    """for multi event ,returns a list; for single event ,returns a dict"""
    return json.loads(open(filepath+'/jsonschema/'+events+'.json', 'r').read())

# def get_expect_event_multi():
#     # import pdb;pdb.set_trace()
#     return events_list.get_expect_event_list()[INFO]


def verify_event(expect):
    print 'test', 'verify_event'
    received_events = get_received_events()
    print '----------'
    print received_events
    print '----------'
    return Verifier().verify_by_jsonschema(expect, received_events)


def verify_event_multi_without_count(expect):
    received_events = get_received_events()
    print '----------'
    print received_events
    print '----------'
    return Verifier().verify_multi_event_without_count(expect, received_events)


# def verify_event_multi():
#     print 'test', 'verify_event'
#     received_events = get_received_events()
#     print '----------'
#     print received_events
#     print get_expect_event_multi()
#     print '----------'
#     return Verifier().verify_multievent(get_expect_event_multi(), received_events)

