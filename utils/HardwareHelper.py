# -*- coding: UTF-8 -*-
from utils import Logger
from utils.adb.AdbHelper import AdbHelper
import time


class HardwareHelper(object):
    """
    硬件操作相关类
    """
    def __init__(self, driver):
        self._driver = driver
        if self._driver is not None and driver.capabilities.has_key('udid'):
            self.udid = self._driver.capabilities['udid']
            self.adbhelper = AdbHelper(self.udid)

    def back(self, times=1, interval=0.5):
        Logger.INFO("clicking hardware back button")
        while times > 0:
            times = times - 1
            self.adbhelper.execute_adb_shell("input keyevent 4")
            time.sleep(interval)
        return True

    def kill_app(self):
        Logger.INFO("killing target app")
        pass

    # 获取屏幕大小
    @property
    def height(self):
        return self._driver.get_window_size()['height']

    @property
    def width(self):
        return self._driver.get_window_size()['width']

    # 百分比滑动
    def scroll_by_percent(self, start_x_per, start_y_per, end_x_per, end_y_per, time = 300):
        try:
            height = self.height
            width = self.width
            start_x = width * start_x_per
            start_y = height * start_y_per
            end_x = width * end_x_per
            end_y = height * end_y_per
            self.adbhelper.execute_adb_shell("input swipe {0} {1} {2} {3}".format(start_x, start_y, end_x, end_y))
            return True
        except BaseException as e:
            Logger.ERROR(e.message)
            Logger.ERROR(e.args)
            return False

    def scroll_up(self, x_per=0.5, start_y_per=0.9, end_y_per=0.1):
        return self.scroll_by_percent(x_per, start_y_per, x_per, end_y_per)

    def scroll_down(self, x_per=0.5, start_y_per=0.2, end_y_per=0.9):
        return self.scroll_by_percent(x_per, start_y_per, x_per, end_y_per)

    def scroll_left(self, y_per=0.5, start_x_per=0.9, end_x_per=0.1):
        return self.scroll_by_percent(start_x_per, y_per, end_x_per, y_per)

    def scroll_right(self, y_per=0.5, start_x_per=0.1, end_x_per=0.9):
        return self.scroll_by_percent(start_x_per, y_per, end_x_per, y_per)

    def scroll_up_one_page(self):
        Logger.INFO("scrolling up one page")
        return self.scroll_up(x_per=0.5,start_y_per=0.9,end_y_per=0.2)

    def scroll_down_one_page(self):
        Logger.INFO("scrolling down one page")
        return self.scroll_up(x_per=0.5,start_y_per=0.2,end_y_per=0.9)








