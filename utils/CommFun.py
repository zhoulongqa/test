# -*- coding: UTF-8 -*-
import sys
from os import path
basedir = path.realpath(path.join(path.dirname(__file__), '../..'))
sys.path.append(basedir)
reload(sys)
import time
import socket
from utils import Logger

def sleep(seconds,addDB = 0,bid = 0):
    Logger.INFO('---------------sleep-----------------%s'%(seconds))
    # if addDB == 1:
    #     from UIAutoTest.Helper.DBHelper import DBHelper
    #     db = DBHelper()
    #     db.insert_into_step_data('','',"等待",bid)
    time.sleep(seconds)

def get_local_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("toutiao.com", 80))
    print s.getsockname()
    return s.getsockname()[0]

def current_time():
    return time.strftime("%Y_%m_%d_%H_%M_%S", time.localtime())

def get_format_time(format='%Y%m%d%H%M%S'):
    return time.strftime(format)



def remove_file(file_path):
    import os
    if os.path.exists(file_path):
        os.remove(file_path)


def get_local_host():
    return ['127.0.0.1', '0.0.0.0']


def has_port_used(port):
    import subprocess
    cmd = "netstat -p tcp -ant | grep '%d '" % port
    print cmd
    try:
        res = subprocess.check_output(cmd, shell=True)
        if res.find(str(port)):
            print '%d is used!' % port
            return True
        else:
            print '%d ---- not used!' % port
            return False
    except:
        print '%d not ++++ used!' % port
        return False


def get_available_port():
    """currently port 10304+ is abandoned on Android/IOS App"""
    base = 9000
    range = 1303
    while True:
        import random
        port = base + int(random.random() * range)
        if not has_port_used(port):
            return port



if __name__ == "__main__":
    has_port_used(16953)
    has_port_used(123)
    has_port_used(9167)