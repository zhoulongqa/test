# -*- coding: UTF-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
# __author__ = 'guohao'
import requests
from time import sleep
# from urllib.parse import quote


class IOSUrlService(object):
    """
    Usage:
        Provide a method to jump to iOS App specified page via url scheme
        such as jump to iOS App shop activity witch shopid=xxx
    """

    def __init__(self, ip, port='18090', app="", home_url=""):
        """
        Usage: init service

        :param ip:  the target phone ip address
        :home_url: go home url will be entry an App root activity
        :param port: the target phone service port, default is: 18090
        """
        self.ip = ip
        self.home_url = home_url
        from utils import Logger
        self.log = Logger
        self.app = app

        self.log.INFO('URL Scheme Service -------->')
        self.log.INFO("IP:" + ip)
        self.log.INFO("Home_Url:" + home_url)
        self.log.INFO('URL Scheme Service <--------')

    def jump_to_page(self, url):
        """
        jump to a page via url scheme
        :param url: url scheme
        :return: the respond of http requests
        """
        # 设置http connection 为关闭状态
        s = requests.session()
        s.keep_alive = False
        if not url:
            self.log.INFO("url is blank, return ")
            return False

        sleep(1)
        full_url = self.get_full_url(url)
        self.log.INFO("full url: " + full_url)
        resp = None
        try:
            resp = requests.get(full_url, timeout=10)
        except Exception as e:
            self.log.ERROR('Entry url failed: ' + full_url)
            self.log.ERROR('Error: '+str(e))
            return False
        finally:
            if resp:
                return True
            else:
                return False

    def get_full_url(self, url):
        """
        Usage:
            convert a url scheme to http requests url
        :param url: url scheme
        :return:  the full path of requests
        """
        return 'http://{ip}:{port}/OpenPage?page={url}'.format(ip=self.ip,
                                                               port=8888,
                                                               url=url)

    def get_encode_url(self, url):
        url_prefix = {
            'com.xxx.xxx': url,
            'com.xxx.xxxtest': url
        }
        return url_prefix[self.app]

    def get_url_service_port(self):
        port_info = {
            'com.xxx.xxx': 8888,
            'com.xxx.xxxtest': 18090
        }
        return port_info[self.app]

    def get_prefix_uri(self):
        url_prefix = {
            'com.xxx.xxx': "OpenPage?page=page://",
            'com.xxx.xxxtest': "FListen?url="
        }
        return url_prefix[self.app]

    def go_home(self):
        if self.home_url:
            self.log.INFO("go to home url scheme")
            self.jump_to_page(self.home_url)
        else:
            self.log.INFO("no home url")
