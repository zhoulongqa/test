# -*- coding: utf-8 -*-
import os
import unittest
from utils.HTMLTestRunner import HTMLTestRunner
from utils.eventutils.EventHttpHandler import EventHttpServer
from utils import CommFun
from testcases.toutiao.CoreEvents.wenda import event_wenda_enter_list, event_wenda_enter_detail

all_cases = [
    event_wenda_enter_detail.event_wenda_enter_detail,
    event_wenda_enter_list.event_wenda_enter_list
]


def select_test_case(case_list):
    test_suite = unittest.TestSuite()
    for case in case_list:
        test_suite.addTest(unittest.makeSuite(case))
    return test_suite


def main():
    test_suite = select_test_case(all_cases)
    # test_suite.addTest(CaseTest.RunTest1('test_01'))
    os.environ['EVENTSERVER'] = 'True'
    # startServer
    EventHttpServer.PORT = 10303
    local_ip = CommFun.get_local_ip()
    EventHttpServer.start_server(local_ip)
    report_fp = 'report/testReport.html'
    html_report = open(report_fp, 'wb')
    runner = HTMLTestRunner(html_report,
                            verbosity=2,
                            title='测试报告',
                            description='执行人admin')
    runner.run(test_suite)


if __name__ == '__main__':
    main()