# __author__ = 'guohao'
import json
import unittest
from utils.AppiumHelper import *
from utils.UIHelper import UIHelper
from utils import Logger
from time import sleep
from utils.URLService import IOSUrlService
from utils import CommFun

class BaseTest(unittest.TestCase):
    driver = None
    adb = None
    uihelper = None

    @classmethod
    def setUpClass(cls):
        Logger.INFO("Init Appium Driver")
        appium_client = AppiumHelper()
        cls.driver = appium_client.get_new_webdriver()
        cls.driver.implicitly_wait(5)
        cls.ios_url_service = IOSUrlService(ip='1')
        cls.uihelper = UIHelper(cls.driver,url_service=cls.ios_url_service,exception_check_json='IOSPopupElements.json')
        sleep(5)

    @classmethod
    def tearDownClass(cls):
        Logger.INFO("Exit Appium Driver")
        cls.driver.quit()