# -*- coding: UTF-8 -*-
import sys

from utils.UIHelper import TTBy
from pages.toutiao import package_name

WendaChannel = {
    '答题' : ("答题", TTBy.TEXT),
    '提问' : ("提问", TTBy.TEXT),
    '发现' : ("发现", TTBy.TEXT),


    # 问答 list
    '普通问答' : package_name + ':id/wd_item_view',

    # 问答item内部view
    '普通问答title' : package_name + ':id/wd_title',
    '普通问答回复数量' : package_name + ':id/wd_info_count',
    '普通问答dislike' : package_name + ':id/right_popicon',

    '视频问答' : package_name + ':id/item_layout',

    # 视频问答item内部view
    '视频问答title' : package_name + ':id/title',
    '视频问答回复数量' : package_name + ':id/answer_num',
    '视频问答dislike' : package_name + ':id/dislike',
    '视频问答播放' : package_name + ':id/video_play_icon'
}

