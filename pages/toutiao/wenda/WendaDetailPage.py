# -*- coding: UTF-8 -*-
import sys
from pages.toutiao import package_name
from utils.UIHelper import TTBy
from pages.toutiao import package_name

WendaDetailPage = {
    # 回答列表
    '悟空问答' : package_name + ':id/question_num_layout',

    # 底部Tab
    '写评论' : package_name + ':id/write_comment_layout',
    'emoji' : package_name + ':id/iv_emoji',
    '查看评论' : package_name + ':id/view_comment_layout',
    '点赞' : package_name + ':id/action_digg',
    '下一个回答' : package_name + ':id/action_repost',


}