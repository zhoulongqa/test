# -*- coding: UTF-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
# __author__ = 'guohao'
from pages.toutiao import package_name
from utils.UIHelper import TTBy


MinePage = {
    '返回' : package_name + ':id/txt_back',

    # 未登录
    '电话登录' : package_name + ':id/img_mobile_btn',
    '火山登录': package_name + ':id/img_huoshan_btn',
    'QQ登录' : package_name + ':id/img_qq_btn',
    '微博登录' : package_name + ':id/img_weibo_btn',
    '更多登录方式' : package_name + ':id/layout_more_platform_with_out_tt_platform_info',
    '今日阅读XX分钟' : package_name + ':id/layout_welfare_with_out_tt_platform_info',

    # 已登录
    '已登陆账号名' : package_name + ':id/txt_user_name',
    '申请认证' : package_name + ":id/button_apply_verify",
    '已登陆头像' : package_name + ":id/view_user_auth",
    '头条公益' : package_name + ":id/layout_welfare_right",

    '收藏' : package_name + ':id/btn_favorite',
    '历史' : package_name + ':id/btn_history',
    '夜间' : package_name + ':id/btn_night_mode',
    '我的关注' : package_name + ':id/mine_my_concerns_layout',
    '消息通知' : ('我的关注', TTBy.TEXT),
    '头条商城' : ('头条商城', TTBy.TEXT),
    '京东特供' : ('京东特供', TTBy.TEXT),
    '用户反馈' : ('用户反馈', TTBy.TEXT),
    '系统设置' : ('系统设置', TTBy.TEXT),

    # 登录页面
    '关闭X号' : package_name + ':id/img_close',
    '手机号输入框' : package_name + ':id/edt_mobile_num',
    '密码输入框' : package_name + ':id/edt_password',
    '账号密码登录' : package_name + ':id/tv_login_with_password',
    '免密码登录' : package_name + ':id/tv_login_with_auth_code',
    "进入头条" : package_name + ':id/btn_confirm',
    '同意CheckBox' : package_name + ':id/user_privacy_clause_checkbox',
}










