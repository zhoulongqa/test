# -*- coding: UTF-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
# __author__ = 'guohao'
from pages.toutiao import package_name
from utils.UIHelper import TTBy


add_category_header = "[add] "
MyCategoryPage = {
    "X" : package_name + ':id/icon_collapse',

    "编辑" : '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout[1]/com.ss.android.account.customview.slidingdrawer.SuperSlidingDrawer/android.widget.RelativeLayout[2]/android.widget.FrameLayout/android.widget.GridView/android.view.View[3]',
    "GridView" : package_name + ':id/dragGridView',
    "频道" : package_name + ':id/text_item',
}
