# -*- coding: UTF-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
from pages.toutiao import package_name
from utils.UIHelper import TTBy


MainPage = {
    # 导航栏
    '未登录' : package_name + ':id/feed_top_search_mine_unlogin_icon',
    '搜索框' : package_name + ':id/search_bar_search_content',
    '发布': package_name + ':id/search_bar_mediamaker_layout',

    # 频道
    '频道条': package_name + ':id/category_strip',
    '更多频道': package_name + ':id/icon_category',

    # Feed流
    # 父控件查找子控件方式定位
    'Feed流列表list' : ('android:id/list', TTBy.ID,'android.widget.LinearLayout', TTBy.CLASS_NAME),
    # 直接通过id定位
    'Feed流列表' : package_name + ':id/root',
    '火山小视频卡片': package_name + ':id/feed_huoshan_card_root_view',

    # 底Tab
    '首页': ('首页', TTBy.TEXT),
    #'西瓜视频': '西瓜视频',
    '西瓜视频': ('西瓜视频', TTBy.TEXT),
    '微头条':('微头条', TTBy.TEXT),
    '小视频':('小视频', TTBy.TEXT),
}

